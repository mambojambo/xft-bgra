# my-libxft

**`master`** branch mirrors upstream `master` branch.

This branch **`my-libxft`** contains custom modifications which are not patched, merged or included in upstream, or made for personal needs. 

Tags in the branch are local to the branch and are used to mark modifications. They are not related to `upstream master` branch tags.

## BGRA | emoji support for DWM, st, dmenu etc

[patch with BGRA support](https://gitlab.freedesktop.org/xorg/lib/libxft/-/commit/b77e5752cbd4acef90904e00c0f392984c321ca9?merge_request_iid=1) is applied here thus **`libXft`** could be build with this option (there is undone [merge request](https://gitlab.freedesktop.org/xorg/lib/libxft/-/merge_requests/1)).


## Void Linux

This lib was specificaly modified to use in Void Linux with DWM window manager, st terminal, and Dmenu. I've build custom packages with `./xbps-src pkg libXft` with patch applied. There is a folder `.voidlinux` in this branch containing modified code as well as `.xbps` builds to easilly install `libXft`.

 
 
